// var config = require('../config').proc;
var gulp = require('gulp');

gulp.task('watch', ['build'], function () {
    'use strict';
    gulp.watch(['bower.json', 'package.json'], ['bower']);
    gulp.watch('src/images/**/*', ['images']);
    gulp.watch('src/png_sprite/**/*', ['png-sprite']);
    gulp.watch('src/svg_sprite/**/*', ['svg-sprite']);
    gulp.watch('src/js/**/*', ['js']);
    gulp.watch(['src/**/*.twig', 'src/**/*.html', 'src/tpl/**/*.json'], ['html']);
    gulp.watch(['src/audio/**/*', 'src/swf/**/*', 'src/video/**/*', 'src/fonts/**/*'], ['files']);
    gulp.watch('src/css/**/*', ['css']);
});