var config = require('../config').css,
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    gulpIf = require('gulp-if'),
    requi = require('gulp-requi'),
    argv = require('yargs').argv;

var errors = require('../helpers/errors');

// CSS & plugins
var stylus = require('gulp-stylus'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano');


// dev mode (если запускать сборку с флагом --dev то статика не будет минифицироваться)
// Пример: $ gulp --dev
gulp.task('css', function () {
    'use strict';

    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(requi())
        .pipe(gulpIf(/[.]styl$/, stylus()))
        .on('error', errors)
        .pipe(autoprefixer({browsers: config.prefix}))
        .pipe(gulpIf(!argv.dev, cssnano({discardComments: {removeAll: true}})))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true, once: true}));
});