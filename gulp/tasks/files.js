var config = require('../config').files,
    gulp = require('gulp');

// audio
gulp.task('audio', function () {
    'use strict';
    return gulp.src([config.src + 'audio/*'])
        .pipe(gulp.dest(config.dest + 'audio/'));
});

// video
gulp.task('video', function () {
    'use strict';
    return gulp.src([config.src + 'video/*'])
        .pipe(gulp.dest(config.dest + 'video/'));
});

// fonts
gulp.task('fonts', function () {
    'use strict';
    return gulp.src(config.src + 'fonts/*')
        .pipe(gulp.dest(config.dest + 'fonts/'));
});

// concat tasks
var taskFiles = ['audio'];
taskFiles.push('video');
taskFiles.push('fonts');

// task: files
gulp.task('files', taskFiles);

