// zip.js
var config = require('../config').zip,
    gulp = require('gulp');

// images plugins
var zip = require('gulp-zip');

gulp.task('zip', function () {
    'use strict';

    return gulp.src(config.src)
        .pipe(zip('arhive.zip'))
        .pipe(gulp.dest(config.dest));
});