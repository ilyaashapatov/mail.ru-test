var path = require('path');

var src = '../src/',
    dist = 'dist/';

module.exports = {
    css: {
        src: path.join(__dirname, src + 'css/*.styl'),
        prefix: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera > 10', 'Explorer >= 9'],
        target: 'style.css',
        dest: dist + 'styles/'
    },

    images: {
        src: path.join(__dirname, src + 'images/**/*'),
        dest: dist + 'images/'
    },

    spriteSvg: {
        src: path.join(__dirname, src + 'svg_sprite/*'),
        dest: 'src/tpl/includes/',
        settings: {
            svgmin: {js2svg: {pretty: true}},
            cheerio: {
                run: function ($) {
                    'use strict';

                    $('[fill]').attr('currentColor');
                    $('[fill-rule]').removeAttr('fill-rule');
                    $('[style]').removeAttr('style');
                    $('title').remove();
                },
                parserOptions: {xmlMode: true}
            },
            sprites: {
                mode: {
                    inline: true,
                    symbol: {
                        dest: './',
                        prefix: 'icon-%s',
                        example: false,
                        sprite: 'sprite.svg'
                    }
                }
            }
        }
    },

    spritePng: {
        sprite: true,

        tmpl: './gulp/helpers/sprite-template.handlebars',

        src: path.join(__dirname, src + 'png_sprite/'),
        dest: 'src/images/',

        mixinPath: 'src/css/variables',
        mixinName: 'sprite-mixins.styl',
        mixinPrefix: 'i-',

        imgName: 'sprite.png',
        imgPath: '../images/sprite.png', // этот путь добавляется в шаблон в background-image url

        retina: false,
        retSrc: path.join(__dirname, src + 'png_sprite/*@2x.png'),
        retName: 'sprite@2x.png',
        retPath: '../images/sprite@2x.png' // этот путь добавляется в шаблон в background-image url
    },


    html: {
        src: path.join(__dirname, src + '*.twig'),
        dest: dist
    },

    js: {
        src: path.join(__dirname, src + 'js/app.js'),
        target: 'app.js',
        dest: dist + 'scripts/'
    },

    files: {
        src: path.join(__dirname, src),
        dest: dist
    },

    bower: {
        targetJs: 'vendor.js',
        destJs: dist + 'scripts/',
        targetCss: '_vendor.styl',
        destCss: 'src/css/modules/'
    },

    zip: {
        src: path.join(__dirname, '../dist/**/*'),
        dest: './'
    }
};