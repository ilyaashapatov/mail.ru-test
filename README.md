Шаблон для быстрого старта верстки проекта

* Gulp
* Twig (html)
* Stylus
* JavaScript
* Sprites
* SVG

=========================================

В качестве сборщика проекта используется `gulp`

## Требования к установке

* [Node.js 0.10+](http://nodejs.org) – это программная платформа, основанная на языке JavaScript и позволяющая легко создавать быстрые и масштабируемые сетевые приложения.
* [Bower.js](http://bower.io/) - установка - `$ npm install -g bower`
* [Git Bash](http://msysgit.github.io/) – для пользователей операционной системы Windows.


## Старт проекта

### Установка

Клонируем репозиторий и устанавливаем все необходимые зависимости:

```bash
git clone git@gitlab.com:ilyaashapatov/html-template.git my-project
cd my-project
npm i && bower i
```

### Запуск

Существует несколько основных команд при работе со сборщиком:

* `gulp` – Запуск сборки проекта с минификацией js, css и изображений. Запускается событие watch. Доступ из браузера по адресу http://localhost:8080/index.html
* `gulp --dev` – Сборка проекта без минификации и с процессом watch. Доступ из браузера по адресу http://localhost:8080/index.html
* `gulp zip` – упаковка содержимого папки `dist` в архив. Архив кладется в корень проекта с названием `archive.zip`


## Настрока конфигурации

Все настройки по проекту вынесены в файл `./gulp/config.js`


### Как работать со спрайтами?

Предусмотрено создание спрайтов в проекте, включение и отключение производится в конфиге `./gulp/config.js` (по умолчанию включено):

```js
    spritePng: {
        sprite: true,
    // code ...
  }
```

Так же предусмотренно создание спрайтов и для retina-экранов. Обратите внимание на следующие настройки (`./gulp/config.js`):
по умолчанию эта опция включена

```js
    spritePng: {
        // code ...
        retina: true,
    }
```

Создем иконку с названием, например `home.png` и кладем ее в папку `src/png_sprite`.

Дальше сборщик все делает сам.

В css коде необходимо прописать только `s-home()` у класса с иконкой.
Название миксина берется из названия иконки и в начало добавляется префикс `s-`

В миксин можно передать параметр `p`:

```
  .class-icon {
    s-home(p) // микисин с параметром p
  }
```

на выходе мы получим

```css
  .class-icon {
    background-position: -5px -5px !important;
  }
```


### SVG sprites

Создем иконку с названием, например `home.svg` и кладем ее в папку `src/svg_sprite`.

И снова сборщик все сделает сам

Используем `<use>` при добвлении иконки в html код

Например
```html
  <!-- `<use>` shape defined ON THIS PAGE somewhere else -->
  <svg viewBox="0 0 100 100">
    <use xlink:href="#home"></use>
  </svg>
```

### Работа с html шаблонами
В проекте работаем с Twig шаблонами
Верстка в Twig практически ничем на отличается от верстки обычным html, за исключением использования отпределнных template тегов.
Подробнее изучить синтаксис Twig можно на [официальном сайте](http://twig.sensiolabs.org/)

Все страницы должны лежать в папке `./src/`.
Шаблоны страниц создем только в том случае, если есть отличия от базового (`./src/tpl/base.twig`) и кладем их в папку `./src/tpl/`
Отдельные блоки/модули должны лежать в папке `./src/tpl/includes/`

### Работа с js

В сборщике есть возможно работать как с обычным js так и писать на CoffeeScript

В директории `./src/js/` мы имеем 3 основных папки:

* scripts - сюда кладутся файлы js которые могу содержать код отдельных модулей, при сборке *все* файлы из этой папки собираются в один и помещаются в конец выходного файла `app.js`
* plugins - сюда кладутся файлы плагинов, которые используются на проекте и подключаются вручную в вверху файла `app.js` через конструкцию `//= require path_to_js_file`
* libs - сюда кладутся js библиотеки и подключаются аналогично как файлы из папки `plugins`

Есть еще один способ добавить плагины в проект - это <b>bower</b>
При использовании <b>bower</b> создается файл `vendor.js` непосредственно в директории `./dist/scripts/`

Добавление бибилиотеки/плагина в проект производится через консоль, командой:

```
$ bower i PLUGIN_NAME --save
```

Обратите внимание на последовательность подключаемых js файлов в проекте:

```
1. vendor.js -- собирается с помощью bower
2. app.js -- собирается из:
  2.1. файлы из папки `./src/js/libs/`
  2.2. файлы из папки `./src/js/plugins/`
  2.3. файлы из папки `./src/js/scripts/`
  2.4. исходный файл `./src/js/app.js`
```

По вопросам работы сборщика а так же верстки в целом, пишите:

* на email: <mailto:ilyaashapatov@uandex.ru>
* в Skype: [ilyaashapatov](skype:ilyaashapatov?chat)